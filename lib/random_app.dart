import 'package:flutter/material.dart';
import 'package:english_words/english_words.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'dart:async';



class RandomWordsState extends State<RandomWords> {
  //class _MyHomePageState extends State<MyHomePage> {
  int page = 1;
  //List<String> items = ['item 1', 'item 2', ];
  final  _suggestions = <WordPair> [];



  final int i=0;
  static int j=0;
  bool isLoading = false;
  @override

  // _buildSuggestions();





  Future _loadData() async {
    // perform fetching data delay
    await new Future.delayed(new Duration(seconds: 2));

    print("load more");
    _choose();
    // update data and loading status

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blueGrey[400],
        title: Text('Random Names',
          style: GoogleFonts.lato(
              textStyle: Theme
                  .of(context)
                  .textTheme
                  .display1,
              fontSize: 28,
              fontWeight: FontWeight.w700,
              color: Colors.white),
        ),
      ),


      body: war(),

    );

  }
  Widget _not() {
    return Column(


      children: <Widget>[


        Expanded(
          child:NotificationListener<ScrollNotification>(
            onNotification: (ScrollNotification scrollInfo) {
              if (!isLoading && scrollInfo.metrics.pixels ==
                  scrollInfo.metrics.maxScrollExtent) {
                _loadData();
                // start loading data
                setState(() {
                  isLoading = true;
                });
              }
            },
            child: _buildSuggestions(),
          )
          ,
        ),

        Container(
          height: isLoading ? 50.0 : 0,
          color: Colors.transparent,
          child: Center(
            child: new CircularProgressIndicator(),
          ),
        ),
      ],
    );
  }








  Widget _buildSuggestions(){
    return Scrollbar(

        child: ListView.builder(
            padding: const EdgeInsets.all(16.0),
            itemCount: _suggestions.length,
            itemBuilder: (context, index) {

              return _buildRow(_suggestions[index]);


            }
        ));


  }
  Widget _buildRow(WordPair pair){
    return Card(
        child:ListTile(
          leading: Icon(FontAwesomeIcons.android),
          title: Text(


            pair.asPascalCase,
            style: GoogleFonts.lato(textStyle: Theme.of(context).textTheme.display1,
                fontSize: 22
            ),

          ),
          trailing: Icon(FontAwesomeIcons.angleRight),
          onTap: () {
            print(Icon(FontAwesomeIcons.boxOpen));
          },

        )
    );

  }
  Widget _choose(){
    setState(() {
      _suggestions.addAll(generateWordPairs().take(15));

      // print('items: '+ _suggestions.toString());//items.addAll( ['item 1']);
      //print('items: '+ items.toString());
      isLoading = false;

    });


  }
  Widget war(){
    if(j==0){
      _choose();
      j=1;
      return _buildSuggestions();
    }
    else{
      return _not();
    }
  }
}
class RandomWords extends StatefulWidget {
  @override
  RandomWordsState createState() => RandomWordsState();

}